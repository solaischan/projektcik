-Posiadanie :
konta na bitbucket

-Następnie sklonowanie repozytorium

-Po otworzeniu i sklonowaniu repozytorium otworzyć je 
w gitpod.io.

-Następnie utworzenie nowej przestrzenii roboczej 

-Rozpocząć konfigurację poprzez edycję pliku konfiguracyjnego config.js 
poprzez wpisanie loginu i hasła oraz numeru telefonu z bazy danych

-W tym samym pliku wstawić adres url przestrzenii roboczej do:
prefix: '/api',
        url: 'https://3000-[przestrzeń robocza]', - bez slasha na końcu
to samo powtórzyć dla frontu:
  front: {
        url: 'https://8080-[przestrzeń robocza]' - również bez slasha

-Następnym krokiem sprawdzenie czy adres w main.ts (front -> src) w 
w linijce adres const socket = io('https://3000:'url-projektu'.gitpod.io/",
pokrywa się z bieżącym adresem url przestrzenii roboczej

- Przy korzystaniu z konsoli należy zainstalować następujące pakiety npm :

a) w webapp main:

npm install --save express

npm i @vue/cli-service

npm install socket.io

b)przejście w konsoli do frontu poprzez cd front i instalacja:

npm install @babel/core @babel/preset-env

npm install socket.io-client

-Dzięki powyższym pakietom możliwe jest uruchomienie frontu
naszej aplikacji poprzez użycie poniższej komendy w folderze front:

npm run serve

-Po poprawnym uruchomieniu frontu, możemy przejść
do uruchomienia backendu z folderu webapp przy wykorzystaniu komendy:

node index.js 

-Funkcja ta spowoduje wywołanie działania backendu,
który to utworzy port nasłuchujący 3000. Odpowiednia
informacja zostanie wyświetlona w konsoli.